
# Rebuild of Top-popcon Debian GNU/Linux 11.x bullseye on amd64

This project rebuilds
[Top-popcon Debian GNU/Linux 11.x bullseye](https://www.debian.org/releases/bullseye/) on
amd64 in a GitLab pipeline, and publish diffoscope
output for any differences.

## Status

We have rebuilt **34%** of
**Top-popcon Debian GNU/Linux 11.x bullseye**!  That is **34%** of the
packages we have built, and we have built **100%** or
**50** of the **50** source packages to rebuild.

Top-popcon Debian GNU/Linux 11.x bullseye (on amd64) contains binary packages
that corresponds to **50** source packages.  Some binary
packages exists in more than one version, so there is a total of
**50** source packages to rebuild.  Of these we have identical
rebuilds for **17** out of the **50**
builds so far.

We have build logs for **50** rebuilds, which may exceed the
number of total source packages to rebuild when a particular source
package (or source package version) has been removed from the archive.
Of the packages we built, **17** packages could be rebuilt
identically, and there are **30** packages that we could
not rebuilt identically.  Building **3** package had build
failures.  We do not attempt to build **0** packages.

[[_TOC_]]

### Rebuildable packages

The following **17** packages could be built locally to
produce the exact same package that is shipped in the archive.

| Package | Version | Build log | Build job |
| ------- | ------- | --------- | --------- |
| adduser | 3.118+deb11u1 | [build Mon Jul  8 23:41:58 UTC 2024](https://gitlab.com/debdistutils/reproduce/debian-bullseye-amd64/-/blob/main/logs/adduser/3.118+deb11u1/buildlog.txt) | [job 7291764245](https://gitlab.com/debdistutils/reproduce/debian-bullseye-amd64/-/jobs/7291764245) |
| base-files | 11.1+deb11u10 | [build Mon Jul  8 23:31:50 UTC 2024](https://gitlab.com/debdistutils/reproduce/debian-bullseye-amd64/-/blob/main/logs/base-files/11.1+deb11u10/buildlog.txt) | [job 7291732858](https://gitlab.com/debdistutils/reproduce/debian-bullseye-amd64/-/jobs/7291732858) |
| base-passwd | 3.5.51 | [build Tue Jul  9 19:40:16 UTC 2024](https://gitlab.com/debdistutils/reproduce/debian-bullseye-amd64/-/blob/main/logs/base-passwd/3.5.51/buildlog.txt) | [job 7301298505](https://gitlab.com/debdistutils/reproduce/debian-bullseye-amd64/-/jobs/7301298505) |
| cpio | 2.13+dfsg-7.1~deb11u1 | [build Mon Jul  8 23:19:58 UTC 2024](https://gitlab.com/debdistutils/reproduce/debian-bullseye-amd64/-/blob/main/logs/cpio/2.13+dfsg-7.1~deb11u1/buildlog.txt) | [job 7291684382](https://gitlab.com/debdistutils/reproduce/debian-bullseye-amd64/-/jobs/7291684382) |
| debconf | 1.5.77 | [build Mon Jul  8 23:34:28 UTC 2024](https://gitlab.com/debdistutils/reproduce/debian-bullseye-amd64/-/blob/main/logs/debconf/1.5.77/buildlog.txt) | [job 7291742488](https://gitlab.com/debdistutils/reproduce/debian-bullseye-amd64/-/jobs/7291742488) |
| debian-archive-keyring | 2021.1.1+deb11u1 | [build Tue Jul  9 08:24:14 UTC 2024](https://gitlab.com/debdistutils/reproduce/debian-bullseye-amd64/-/blob/main/logs/debian-archive-keyring/2021.1.1+deb11u1/buildlog.txt) | [job 7294527468](https://gitlab.com/debdistutils/reproduce/debian-bullseye-amd64/-/jobs/7294527468) |
| dpkg | 1.20.13 | [build Mon Jul  8 23:19:34 UTC 2024](https://gitlab.com/debdistutils/reproduce/debian-bullseye-amd64/-/blob/main/logs/dpkg/1.20.13/buildlog.txt) | [job 7291684350](https://gitlab.com/debdistutils/reproduce/debian-bullseye-amd64/-/jobs/7291684350) |
| expat | 2.2.10-2+deb11u5 | [build Mon Jul  8 23:12:17 UTC 2024](https://gitlab.com/debdistutils/reproduce/debian-bullseye-amd64/-/blob/main/logs/expat/2.2.10-2+deb11u5/buildlog.txt) | [job 7291652233](https://gitlab.com/debdistutils/reproduce/debian-bullseye-amd64/-/jobs/7291652233) |
| grep | 3.6-1+deb11u1 | [build Mon Jul  8 23:44:01 UTC 2024](https://gitlab.com/debdistutils/reproduce/debian-bullseye-amd64/-/blob/main/logs/grep/3.6-1+deb11u1/buildlog.txt) | [job 7291769930](https://gitlab.com/debdistutils/reproduce/debian-bullseye-amd64/-/jobs/7291769930) |
| gzip | 1.10-4+deb11u1 | [build Mon Jul  8 23:56:27 UTC 2024](https://gitlab.com/debdistutils/reproduce/debian-bullseye-amd64/-/blob/main/logs/gzip/1.10-4+deb11u1/buildlog.txt) | [job 7291802510](https://gitlab.com/debdistutils/reproduce/debian-bullseye-amd64/-/jobs/7291802510) |
| logrotate | 3.18.0-2+deb11u2 | [build Mon Jul  8 23:40:28 UTC 2024](https://gitlab.com/debdistutils/reproduce/debian-bullseye-amd64/-/blob/main/logs/logrotate/3.18.0-2+deb11u2/buildlog.txt) | [job 7291759730](https://gitlab.com/debdistutils/reproduce/debian-bullseye-amd64/-/jobs/7291759730) |
| ncurses | 6.2+20201114-2+deb11u2 | [build Mon Jul  8 23:34:27 UTC 2024](https://gitlab.com/debdistutils/reproduce/debian-bullseye-amd64/-/blob/main/logs/ncurses/6.2+20201114-2+deb11u2/buildlog.txt) | [job 7291742489](https://gitlab.com/debdistutils/reproduce/debian-bullseye-amd64/-/jobs/7291742489) |
| netbase | 6.3 | [build Mon Jul  8 23:34:32 UTC 2024](https://gitlab.com/debdistutils/reproduce/debian-bullseye-amd64/-/blob/main/logs/netbase/6.3/buildlog.txt) | [job 7291742490](https://gitlab.com/debdistutils/reproduce/debian-bullseye-amd64/-/jobs/7291742490) |
| pam | 1.4.0-9+deb11u1 | [build Mon Jul  8 23:24:19 UTC 2024](https://gitlab.com/debdistutils/reproduce/debian-bullseye-amd64/-/blob/main/logs/pam/1.4.0-9+deb11u1/buildlog.txt) | [job 7291703946](https://gitlab.com/debdistutils/reproduce/debian-bullseye-amd64/-/jobs/7291703946) |
| sysvinit | 2.96-7+deb11u1 | [build Mon Jul  8 23:12:20 UTC 2024](https://gitlab.com/debdistutils/reproduce/debian-bullseye-amd64/-/blob/main/logs/sysvinit/2.96-7+deb11u1/buildlog.txt) | [job 7291652232](https://gitlab.com/debdistutils/reproduce/debian-bullseye-amd64/-/jobs/7291652232) |
| tzdata | 2024a-0+deb11u1 | [build Mon Jul  8 23:31:35 UTC 2024](https://gitlab.com/debdistutils/reproduce/debian-bullseye-amd64/-/blob/main/logs/tzdata/2024a-0+deb11u1/buildlog.txt) | [job 7291732859](https://gitlab.com/debdistutils/reproduce/debian-bullseye-amd64/-/jobs/7291732859) |
| util-linux | 2.36.1-8+deb11u2 | [build Mon Jul  8 23:20:13 UTC 2024](https://gitlab.com/debdistutils/reproduce/debian-bullseye-amd64/-/blob/main/logs/util-linux/2.36.1-8+deb11u2/buildlog.txt) | [job 7291684366](https://gitlab.com/debdistutils/reproduce/debian-bullseye-amd64/-/jobs/7291684366) |

### Build failures

The following **3** packages have build failures, making it
impossible to even compare the binary package in the archive with what
we are able to build locally.

| Package | Version | Build log | Build job |
| ------- | ------- | --------- | --------- |
| coreutils | 8.32-4 | [build Mon Jul  8 23:12:06 UTC 2024](https://gitlab.com/debdistutils/reproduce/debian-bullseye-amd64/-/blob/main/logs/coreutils/8.32-4/buildlog.txt) | [job 7291652230](https://gitlab.com/debdistutils/reproduce/debian-bullseye-amd64/-/jobs/7291652230) |
| readline | 8.1-1 | [build Mon Jul  8 23:40:18 UTC 2024](https://gitlab.com/debdistutils/reproduce/debian-bullseye-amd64/-/blob/main/logs/readline/8.1-1/buildlog.txt) | [job 7291758917](https://gitlab.com/debdistutils/reproduce/debian-bullseye-amd64/-/jobs/7291758917) |
| tar | 1.34+dfsg-1+deb11u1 | [build Mon Jul  8 23:40:07 UTC 2024](https://gitlab.com/debdistutils/reproduce/debian-bullseye-amd64/-/blob/main/logs/tar/1.34+dfsg-1+deb11u1/buildlog.txt) | [job 7291758915](https://gitlab.com/debdistutils/reproduce/debian-bullseye-amd64/-/jobs/7291758915) |

### Unrebuildable packages

The following **30** packages unfortunately differ in
some way compared to the version distributed in the archive.  Please
investigate the build log and help us improve things!

| Package | Version | Build log | Jobs | Diffoscope |
| ------- | ------- | --------- | ---- | ---------- |
| acl | 2.2.53-10 | [log](https://gitlab.com/debdistutils/reproduce/debian-bullseye-amd64/-/blob/main/logs/acl/2.2.53-10/buildlog.txt) | build [7291759727](https://gitlab.com/debdistutils/reproduce/debian-bullseye-amd64/-/jobs/7291759727) <br> diff [7291759732](https://gitlab.com/debdistutils/reproduce/debian-bullseye-amd64/-/jobs/7291759732) | [diff 7291759732](https://debdistutils.gitlab.io/-/reproduce/debian-bullseye-amd64/-/jobs/7291759732/artifacts/diffoscope/index.html) |
| apt | 2.2.4 | [log](https://gitlab.com/debdistutils/reproduce/debian-bullseye-amd64/-/blob/main/logs/apt/2.2.4/buildlog.txt) | build [7294527462](https://gitlab.com/debdistutils/reproduce/debian-bullseye-amd64/-/jobs/7294527462) <br> diff [7294527482](https://gitlab.com/debdistutils/reproduce/debian-bullseye-amd64/-/jobs/7294527482) | [diff 7294527482](https://debdistutils.gitlab.io/-/reproduce/debian-bullseye-amd64/-/jobs/7294527482/artifacts/diffoscope/index.html) |
| attr | 1:2.4.48-6 | [log](https://gitlab.com/debdistutils/reproduce/debian-bullseye-amd64/-/blob/main/logs/attr/1:2.4.48-6/buildlog.txt) | build [7291732856](https://gitlab.com/debdistutils/reproduce/debian-bullseye-amd64/-/jobs/7291732856) <br> diff [7291732865](https://gitlab.com/debdistutils/reproduce/debian-bullseye-amd64/-/jobs/7291732865) | [diff 7291732865](https://debdistutils.gitlab.io/-/reproduce/debian-bullseye-amd64/-/jobs/7291732865/artifacts/diffoscope/index.html) |
| bash | 5.1-2+deb11u1 | [log](https://gitlab.com/debdistutils/reproduce/debian-bullseye-amd64/-/blob/main/logs/bash/5.1-2+deb11u1/buildlog.txt) | build [7294527465](https://gitlab.com/debdistutils/reproduce/debian-bullseye-amd64/-/jobs/7294527465) <br> diff [7294527486](https://gitlab.com/debdistutils/reproduce/debian-bullseye-amd64/-/jobs/7294527486) | [diff 7294527486](https://debdistutils.gitlab.io/-/reproduce/debian-bullseye-amd64/-/jobs/7294527486/artifacts/diffoscope/index.html) |
| bzip2 | 1.0.8-4 | [log](https://gitlab.com/debdistutils/reproduce/debian-bullseye-amd64/-/blob/main/logs/bzip2/1.0.8-4/buildlog.txt) | build [7291732855](https://gitlab.com/debdistutils/reproduce/debian-bullseye-amd64/-/jobs/7291732855) <br> diff [7291732861](https://gitlab.com/debdistutils/reproduce/debian-bullseye-amd64/-/jobs/7291732861) | [diff 7291732861](https://debdistutils.gitlab.io/-/reproduce/debian-bullseye-amd64/-/jobs/7291732861/artifacts/diffoscope/index.html) |
| e2fsprogs | 1.46.2-2 | [log](https://gitlab.com/debdistutils/reproduce/debian-bullseye-amd64/-/blob/main/logs/e2fsprogs/1.46.2-2/buildlog.txt) | build [7291769931](https://gitlab.com/debdistutils/reproduce/debian-bullseye-amd64/-/jobs/7291769931) <br> diff [7291769937](https://gitlab.com/debdistutils/reproduce/debian-bullseye-amd64/-/jobs/7291769937) | [diff 7291769937](https://debdistutils.gitlab.io/-/reproduce/debian-bullseye-amd64/-/jobs/7291769937/artifacts/diffoscope/index.html) |
| findutils | 4.8.0-1 | [log](https://gitlab.com/debdistutils/reproduce/debian-bullseye-amd64/-/blob/main/logs/findutils/4.8.0-1/buildlog.txt) | build [7291759728](https://gitlab.com/debdistutils/reproduce/debian-bullseye-amd64/-/jobs/7291759728) <br> diff [7291759733](https://gitlab.com/debdistutils/reproduce/debian-bullseye-amd64/-/jobs/7291759733) | [diff 7291759733](https://debdistutils.gitlab.io/-/reproduce/debian-bullseye-amd64/-/jobs/7291759733/artifacts/diffoscope/index.html) |
| gettext | 0.21-4 | [log](https://gitlab.com/debdistutils/reproduce/debian-bullseye-amd64/-/blob/main/logs/gettext/0.21-4/buildlog.txt) | build [7291652234](https://gitlab.com/debdistutils/reproduce/debian-bullseye-amd64/-/jobs/7291652234) <br> diff [7291652243](https://gitlab.com/debdistutils/reproduce/debian-bullseye-amd64/-/jobs/7291652243) | [diff 7291652243](https://debdistutils.gitlab.io/-/reproduce/debian-bullseye-amd64/-/jobs/7291652243/artifacts/diffoscope/index.html) |
| glibc | 2.31-13+deb11u10 | [log](https://gitlab.com/debdistutils/reproduce/debian-bullseye-amd64/-/blob/main/logs/glibc/2.31-13+deb11u10/buildlog.txt) | build [7291764247](https://gitlab.com/debdistutils/reproduce/debian-bullseye-amd64/-/jobs/7291764247) <br> diff [7291764252](https://gitlab.com/debdistutils/reproduce/debian-bullseye-amd64/-/jobs/7291764252) | [diff 7291764252](https://debdistutils.gitlab.io/-/reproduce/debian-bullseye-amd64/-/jobs/7291764252/artifacts/diffoscope/index.html) |
| gnupg2 | 2.2.27-2+deb11u2 | [log](https://gitlab.com/debdistutils/reproduce/debian-bullseye-amd64/-/blob/main/logs/gnupg2/2.2.27-2+deb11u2/buildlog.txt) | build [7291703947](https://gitlab.com/debdistutils/reproduce/debian-bullseye-amd64/-/jobs/7291703947) <br> diff [7291703958](https://gitlab.com/debdistutils/reproduce/debian-bullseye-amd64/-/jobs/7291703958) | [diff 7291703958](https://debdistutils.gitlab.io/-/reproduce/debian-bullseye-amd64/-/jobs/7291703958/artifacts/diffoscope/index.html) |
| hostname | 3.23 | [log](https://gitlab.com/debdistutils/reproduce/debian-bullseye-amd64/-/blob/main/logs/hostname/3.23/buildlog.txt) | build [7291764246](https://gitlab.com/debdistutils/reproduce/debian-bullseye-amd64/-/jobs/7291764246) <br> diff [7291764251](https://gitlab.com/debdistutils/reproduce/debian-bullseye-amd64/-/jobs/7291764251) | [diff 7291764251](https://debdistutils.gitlab.io/-/reproduce/debian-bullseye-amd64/-/jobs/7291764251/artifacts/diffoscope/index.html) |
| iputils | 3:20210202-1 | [log](https://gitlab.com/debdistutils/reproduce/debian-bullseye-amd64/-/blob/main/logs/iputils/3:20210202-1/buildlog.txt) | build [7291684403](https://gitlab.com/debdistutils/reproduce/debian-bullseye-amd64/-/jobs/7291684403) <br> diff [7291684506](https://gitlab.com/debdistutils/reproduce/debian-bullseye-amd64/-/jobs/7291684506) | [diff 7291684506](https://debdistutils.gitlab.io/-/reproduce/debian-bullseye-amd64/-/jobs/7291684506/artifacts/diffoscope/index.html) |
| keyutils | 1.6.1-2 | [log](https://gitlab.com/debdistutils/reproduce/debian-bullseye-amd64/-/blob/main/logs/keyutils/1.6.1-2/buildlog.txt) | build [7291703950](https://gitlab.com/debdistutils/reproduce/debian-bullseye-amd64/-/jobs/7291703950) <br> diff [7291703960](https://gitlab.com/debdistutils/reproduce/debian-bullseye-amd64/-/jobs/7291703960) | [diff 7291703960](https://debdistutils.gitlab.io/-/reproduce/debian-bullseye-amd64/-/jobs/7291703960/artifacts/diffoscope/index.html) |
| libcap2 | 1:2.44-1 | [log](https://gitlab.com/debdistutils/reproduce/debian-bullseye-amd64/-/blob/main/logs/libcap2/1:2.44-1/buildlog.txt) | [build 7291758918](https://gitlab.com/debdistutils/reproduce/debian-bullseye-amd64/-/jobs/7291758918) | missing |
| libedit | 3.1-20191231-2 | [log](https://gitlab.com/debdistutils/reproduce/debian-bullseye-amd64/-/blob/main/logs/libedit/3.1-20191231-2/buildlog.txt) | build [7291901576](https://gitlab.com/debdistutils/reproduce/debian-bullseye-amd64/-/jobs/7291901576) <br> diff [7291901583](https://gitlab.com/debdistutils/reproduce/debian-bullseye-amd64/-/jobs/7291901583) | [diff 7291901583](https://debdistutils.gitlab.io/-/reproduce/debian-bullseye-amd64/-/jobs/7291901583/artifacts/diffoscope/index.html) |
| libgpg-error | 1.38-2 | [log](https://gitlab.com/debdistutils/reproduce/debian-bullseye-amd64/-/blob/main/logs/libgpg-error/1.38-2/buildlog.txt) | build [7291652231](https://gitlab.com/debdistutils/reproduce/debian-bullseye-amd64/-/jobs/7291652231) <br> diff [7291652236](https://gitlab.com/debdistutils/reproduce/debian-bullseye-amd64/-/jobs/7291652236) | [diff 7291652236](https://debdistutils.gitlab.io/-/reproduce/debian-bullseye-amd64/-/jobs/7291652236/artifacts/diffoscope/index.html) |
| liblocale-gettext-perl | 1.07-4 | [log](https://gitlab.com/debdistutils/reproduce/debian-bullseye-amd64/-/blob/main/logs/liblocale-gettext-perl/1.07-4/buildlog.txt) | build [7294527471](https://gitlab.com/debdistutils/reproduce/debian-bullseye-amd64/-/jobs/7294527471) <br> diff [7294527490](https://gitlab.com/debdistutils/reproduce/debian-bullseye-amd64/-/jobs/7294527490) | [diff 7294527490](https://debdistutils.gitlab.io/-/reproduce/debian-bullseye-amd64/-/jobs/7294527490/artifacts/diffoscope/index.html) |
| libselinux | 3.1-3 | [log](https://gitlab.com/debdistutils/reproduce/debian-bullseye-amd64/-/blob/main/logs/libselinux/3.1-3/buildlog.txt) | build [7291684369](https://gitlab.com/debdistutils/reproduce/debian-bullseye-amd64/-/jobs/7291684369) <br> diff [7291684468](https://gitlab.com/debdistutils/reproduce/debian-bullseye-amd64/-/jobs/7291684468) | [diff 7291684468](https://debdistutils.gitlab.io/-/reproduce/debian-bullseye-amd64/-/jobs/7291684468/artifacts/diffoscope/index.html) |
| lvm2 | 2.03.11-2.1 | [log](https://gitlab.com/debdistutils/reproduce/debian-bullseye-amd64/-/blob/main/logs/lvm2/2.03.11-2.1/buildlog.txt) | build [7291764248](https://gitlab.com/debdistutils/reproduce/debian-bullseye-amd64/-/jobs/7291764248) <br> diff [7291764253](https://gitlab.com/debdistutils/reproduce/debian-bullseye-amd64/-/jobs/7291764253) | [diff 7291764253](https://debdistutils.gitlab.io/-/reproduce/debian-bullseye-amd64/-/jobs/7291764253/artifacts/diffoscope/index.html) |
| mawk | 1.3.4.20200120-2 | [log](https://gitlab.com/debdistutils/reproduce/debian-bullseye-amd64/-/blob/main/logs/mawk/1.3.4.20200120-2/buildlog.txt) | build [7291732860](https://gitlab.com/debdistutils/reproduce/debian-bullseye-amd64/-/jobs/7291732860) <br> diff [7291732874](https://gitlab.com/debdistutils/reproduce/debian-bullseye-amd64/-/jobs/7291732874) | [diff 7291732874](https://debdistutils.gitlab.io/-/reproduce/debian-bullseye-amd64/-/jobs/7291732874/artifacts/diffoscope/index.html) |
| newt | 0.52.21-4 | [log](https://gitlab.com/debdistutils/reproduce/debian-bullseye-amd64/-/blob/main/logs/newt/0.52.21-4/buildlog.txt) | build [7291703952](https://gitlab.com/debdistutils/reproduce/debian-bullseye-amd64/-/jobs/7291703952) <br> diff [7291703961](https://gitlab.com/debdistutils/reproduce/debian-bullseye-amd64/-/jobs/7291703961) | [diff 7291703961](https://debdistutils.gitlab.io/-/reproduce/debian-bullseye-amd64/-/jobs/7291703961/artifacts/diffoscope/index.html) |
| perl | 5.32.1-4+deb11u3 | [log](https://gitlab.com/debdistutils/reproduce/debian-bullseye-amd64/-/blob/main/logs/perl/5.32.1-4+deb11u3/buildlog.txt) | build [7291769929](https://gitlab.com/debdistutils/reproduce/debian-bullseye-amd64/-/jobs/7291769929) <br> diff [7291769934](https://gitlab.com/debdistutils/reproduce/debian-bullseye-amd64/-/jobs/7291769934) | [diff 7291769934](https://debdistutils.gitlab.io/-/reproduce/debian-bullseye-amd64/-/jobs/7291769934/artifacts/diffoscope/index.html) |
| popt | 1.18-2 | [log](https://gitlab.com/debdistutils/reproduce/debian-bullseye-amd64/-/blob/main/logs/popt/1.18-2/buildlog.txt) | build [7291703949](https://gitlab.com/debdistutils/reproduce/debian-bullseye-amd64/-/jobs/7291703949) <br> diff [7291703959](https://gitlab.com/debdistutils/reproduce/debian-bullseye-amd64/-/jobs/7291703959) | [diff 7291703959](https://debdistutils.gitlab.io/-/reproduce/debian-bullseye-amd64/-/jobs/7291703959/artifacts/diffoscope/index.html) |
| popularity-contest | 1.71 | [log](https://gitlab.com/debdistutils/reproduce/debian-bullseye-amd64/-/blob/main/logs/popularity-contest/1.71/buildlog.txt) | build [7291742491](https://gitlab.com/debdistutils/reproduce/debian-bullseye-amd64/-/jobs/7291742491) <br> diff [7291742497](https://gitlab.com/debdistutils/reproduce/debian-bullseye-amd64/-/jobs/7291742497) | [diff 7291742497](https://debdistutils.gitlab.io/-/reproduce/debian-bullseye-amd64/-/jobs/7291742497/artifacts/diffoscope/index.html) |
| procps | 2:3.3.17-5 | [log](https://gitlab.com/debdistutils/reproduce/debian-bullseye-amd64/-/blob/main/logs/procps/2:3.3.17-5/buildlog.txt) | build [7291878429](https://gitlab.com/debdistutils/reproduce/debian-bullseye-amd64/-/jobs/7291878429) <br> diff [7291878435](https://gitlab.com/debdistutils/reproduce/debian-bullseye-amd64/-/jobs/7291878435) | [diff 7291878435](https://debdistutils.gitlab.io/-/reproduce/debian-bullseye-amd64/-/jobs/7291878435/artifacts/diffoscope/index.html) |
| sed | 4.7-1 | [log](https://gitlab.com/debdistutils/reproduce/debian-bullseye-amd64/-/blob/main/logs/sed/4.7-1/buildlog.txt) | build [7291901574](https://gitlab.com/debdistutils/reproduce/debian-bullseye-amd64/-/jobs/7291901574) <br> diff [7291901581](https://gitlab.com/debdistutils/reproduce/debian-bullseye-amd64/-/jobs/7291901581) | [diff 7291901581](https://debdistutils.gitlab.io/-/reproduce/debian-bullseye-amd64/-/jobs/7291901581/artifacts/diffoscope/index.html) |
| shadow | 1:4.8.1-1 | [log](https://gitlab.com/debdistutils/reproduce/debian-bullseye-amd64/-/blob/main/logs/shadow/1:4.8.1-1/buildlog.txt) | build [7291802509](https://gitlab.com/debdistutils/reproduce/debian-bullseye-amd64/-/jobs/7291802509) <br> diff [7291802514](https://gitlab.com/debdistutils/reproduce/debian-bullseye-amd64/-/jobs/7291802514) | [diff 7291802514](https://debdistutils.gitlab.io/-/reproduce/debian-bullseye-amd64/-/jobs/7291802514/artifacts/diffoscope/index.html) |
| slang2 | 2.3.2-5 | [log](https://gitlab.com/debdistutils/reproduce/debian-bullseye-amd64/-/blob/main/logs/slang2/2.3.2-5/buildlog.txt) | build [7301170960](https://gitlab.com/debdistutils/reproduce/debian-bullseye-amd64/-/jobs/7301170960) <br> diff [7301170972](https://gitlab.com/debdistutils/reproduce/debian-bullseye-amd64/-/jobs/7301170972) | [diff 7301170972](https://debdistutils.gitlab.io/-/reproduce/debian-bullseye-amd64/-/jobs/7301170972/artifacts/diffoscope/index.html) |
| ucf | 3.0043 | [log](https://gitlab.com/debdistutils/reproduce/debian-bullseye-amd64/-/blob/main/logs/ucf/3.0043/buildlog.txt) | build [7291759729](https://gitlab.com/debdistutils/reproduce/debian-bullseye-amd64/-/jobs/7291759729) <br> diff [7291759734](https://gitlab.com/debdistutils/reproduce/debian-bullseye-amd64/-/jobs/7291759734) | [diff 7291759734](https://debdistutils.gitlab.io/-/reproduce/debian-bullseye-amd64/-/jobs/7291759734/artifacts/diffoscope/index.html) |
| zlib | 1:1.2.11.dfsg-2+deb11u2 | [log](https://gitlab.com/debdistutils/reproduce/debian-bullseye-amd64/-/blob/main/logs/zlib/1:1.2.11.dfsg-2+deb11u2/buildlog.txt) | build [7291878428](https://gitlab.com/debdistutils/reproduce/debian-bullseye-amd64/-/jobs/7291878428) <br> diff [7291878434](https://gitlab.com/debdistutils/reproduce/debian-bullseye-amd64/-/jobs/7291878434) | [diff 7291878434](https://debdistutils.gitlab.io/-/reproduce/debian-bullseye-amd64/-/jobs/7291878434/artifacts/diffoscope/index.html) |

### Timestamps

The timestamps of the archives used as input to find out which
packages to reproduce are as follows.  To be precise, these are the
`Date` field in the respectively `Release` file used to construct
the list of packages to evaluate.

When speaking about "current" status of this effort it makes sense to
use the latest timestamp from the set below, which is
**Mon Jul  8 20:22:01 UTC 2024**.

| Archive/Suite | Timestamp |
| ------------- | --------- |
| debian/bullseye | Sat, 29 Jun 2024 10:26:51 UTC |
| debian/bullseye-updates | Mon, 08 Jul 2024 20:22:01 UTC |
| debian-security/bullseye-security | Sun, 07 Jul 2024 12:12:17 UTC |

## License

The content of this repository is automatically generated by
[debdistrebuild](https://gitlab.com/debdistutils/debdistrebuild),
which is published under the
[AGPLv3+](https://www.gnu.org/licenses/agpl-3.0.en.html) and to the
extent the outputs are copyrightable they are released under the same
license.

## Contact

The maintainer of this project is [Simon
Josefsson](https://blog.josefsson.org/).

