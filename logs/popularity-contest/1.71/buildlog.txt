+ date
Mon Jul  8 23:34:44 UTC 2024
+ id
uid=0(root) gid=0(root) groups=0(root)
+ pwd
/build/popularity-contest
+ apt-get source --only-source popularity-contest=1.71
Reading package lists...
NOTICE: 'popularity-contest' packaging is maintained in the 'Git' version control system at:
https://salsa.debian.org/popularity-contest-team/popularity-contest.git
Please use:
git clone https://salsa.debian.org/popularity-contest-team/popularity-contest.git
to retrieve the latest (possibly unreleased) updates to the package.
Need to get 79.6 kB of source archives.
Get:1 http://deb.debian.org/debian bullseye/main popularity-contest 1.71 (dsc) [1731 B]
Get:2 http://deb.debian.org/debian bullseye/main popularity-contest 1.71 (tar) [77.9 kB]
dpkg-source: info: extracting popularity-contest in popularity-contest-1.71
dpkg-source: info: unpacking popularity-contest_1.71.tar.xz
Fetched 79.6 kB in 0s (225 kB/s)
W: Download is performed unsandboxed as root as file 'popularity-contest_1.71.dsc' couldn't be accessed by user '_apt'. - pkgAcquire::Run (13: Permission denied)
+ ls -la
total 96
drwxr-xr-x 3 root root  4096 Jul  8 23:34 .
drwxr-xr-x 3 root root  4096 Jul  8 23:34 ..
drwxr-xr-x 4 root root  4096 Dec 12  2020 popularity-contest-1.71
-rw-r--r-- 1 root root  1731 Dec 12  2020 popularity-contest_1.71.dsc
-rw-r--r-- 1 root root 77896 Dec 12  2020 popularity-contest_1.71.tar.xz
+ find . -maxdepth 1 -type f
+ sha256sum ./popularity-contest_1.71.tar.xz ./popularity-contest_1.71.dsc
e770008b07240c35c5ed86aa72f1a4ee4c16909f27c8030a6b3a9e0e9c8a01bd  ./popularity-contest_1.71.tar.xz
d631d743a37d64a70ba2df6d59340ffb038245ba1e9c2ef2e2982eea1ffbb632  ./popularity-contest_1.71.dsc
+ find . -maxdepth 1 -name popularity-contest* -type d
+ cd ./popularity-contest-1.71
+ env DEBIAN_FRONTEND=noninteractive apt-get build-dep -y --only-source popularity-contest=1.71
Reading package lists...
Building dependency tree...
Reading state information...
The following NEW packages will be installed:
  autoconf automake autopoint autotools-dev bsdextrautils debhelper
  dh-autoreconf dh-strip-nondeterminism dwz gettext gettext-base groff-base
  intltool-debian libarchive-zip-perl libdebhelper-perl libelf1
  libfile-stripnondeterminism-perl libicu67 libpipeline1 libsigsegv2
  libsub-override-perl libtool libuchardet0 libxml2 m4 man-db po-debconf
0 upgraded, 27 newly installed, 0 to remove and 0 not upgraded.
Need to get 17.8 MB of archives.
After this operation, 60.6 MB of additional disk space will be used.
Get:1 http://deb.debian.org/debian bullseye/main amd64 bsdextrautils amd64 2.36.1-8+deb11u2 [146 kB]
Get:2 http://deb.debian.org/debian bullseye/main amd64 libuchardet0 amd64 0.0.7-1 [67.8 kB]
Get:3 http://deb.debian.org/debian bullseye/main amd64 groff-base amd64 1.22.4-6 [936 kB]
Get:4 http://deb.debian.org/debian bullseye/main amd64 libpipeline1 amd64 1.5.3-1 [34.3 kB]
Get:5 http://deb.debian.org/debian bullseye/main amd64 man-db amd64 2.9.4-2 [1354 kB]
Get:6 http://deb.debian.org/debian bullseye/main amd64 gettext-base amd64 0.21-4 [175 kB]
Get:7 http://deb.debian.org/debian bullseye/main amd64 libsigsegv2 amd64 2.13-1 [34.8 kB]
Get:8 http://deb.debian.org/debian bullseye/main amd64 m4 amd64 1.4.18-5 [204 kB]
Get:9 http://deb.debian.org/debian bullseye/main amd64 autoconf all 2.69-14 [313 kB]
Get:10 http://deb.debian.org/debian bullseye/main amd64 autotools-dev all 20180224.1+nmu1 [77.1 kB]
Get:11 http://deb.debian.org/debian bullseye/main amd64 automake all 1:1.16.3-2 [814 kB]
Get:12 http://deb.debian.org/debian bullseye/main amd64 autopoint all 0.21-4 [510 kB]
Get:13 http://deb.debian.org/debian bullseye/main amd64 libdebhelper-perl all 13.3.4 [189 kB]
Get:14 http://deb.debian.org/debian bullseye/main amd64 libtool all 2.4.6-15 [513 kB]
Get:15 http://deb.debian.org/debian bullseye/main amd64 dh-autoreconf all 20 [17.1 kB]
Get:16 http://deb.debian.org/debian bullseye/main amd64 libarchive-zip-perl all 1.68-1 [104 kB]
Get:17 http://deb.debian.org/debian bullseye/main amd64 libsub-override-perl all 0.09-2 [10.2 kB]
Get:18 http://deb.debian.org/debian bullseye/main amd64 libfile-stripnondeterminism-perl all 1.12.0-1 [26.3 kB]
Get:19 http://deb.debian.org/debian bullseye/main amd64 dh-strip-nondeterminism all 1.12.0-1 [15.4 kB]
Get:20 http://deb.debian.org/debian bullseye/main amd64 libelf1 amd64 0.183-1 [165 kB]
Get:21 http://deb.debian.org/debian bullseye/main amd64 dwz amd64 0.13+20210201-1 [175 kB]
Get:22 http://deb.debian.org/debian bullseye/main amd64 libicu67 amd64 67.1-7 [8622 kB]
Get:23 http://deb.debian.org/debian bullseye/main amd64 libxml2 amd64 2.9.10+dfsg-6.7+deb11u4 [693 kB]
Get:24 http://deb.debian.org/debian bullseye/main amd64 gettext amd64 0.21-4 [1311 kB]
Get:25 http://deb.debian.org/debian bullseye/main amd64 intltool-debian all 0.35.0+20060710.5 [26.8 kB]
Get:26 http://deb.debian.org/debian bullseye/main amd64 po-debconf all 1.0.21+nmu1 [248 kB]
Get:27 http://deb.debian.org/debian bullseye/main amd64 debhelper all 13.3.4 [1049 kB]
debconf: delaying package configuration, since apt-utils is not installed
Fetched 17.8 MB in 0s (66.3 MB/s)
Selecting previously unselected package bsdextrautils.
(Reading database ... (Reading database ... 5%(Reading database ... 10%(Reading database ... 15%(Reading database ... 20%(Reading database ... 25%(Reading database ... 30%(Reading database ... 35%(Reading database ... 40%(Reading database ... 45%(Reading database ... 50%(Reading database ... 55%(Reading database ... 60%(Reading database ... 65%(Reading database ... 70%(Reading database ... 75%(Reading database ... 80%(Reading database ... 85%(Reading database ... 90%(Reading database ... 95%(Reading database ... 100%(Reading database ... 16217 files and directories currently installed.)
Preparing to unpack .../00-bsdextrautils_2.36.1-8+deb11u2_amd64.deb ...
Unpacking bsdextrautils (2.36.1-8+deb11u2) ...
Selecting previously unselected package libuchardet0:amd64.
Preparing to unpack .../01-libuchardet0_0.0.7-1_amd64.deb ...
Unpacking libuchardet0:amd64 (0.0.7-1) ...
Selecting previously unselected package groff-base.
Preparing to unpack .../02-groff-base_1.22.4-6_amd64.deb ...
Unpacking groff-base (1.22.4-6) ...
Selecting previously unselected package libpipeline1:amd64.
Preparing to unpack .../03-libpipeline1_1.5.3-1_amd64.deb ...
Unpacking libpipeline1:amd64 (1.5.3-1) ...
Selecting previously unselected package man-db.
Preparing to unpack .../04-man-db_2.9.4-2_amd64.deb ...
Unpacking man-db (2.9.4-2) ...
Selecting previously unselected package gettext-base.
Preparing to unpack .../05-gettext-base_0.21-4_amd64.deb ...
Unpacking gettext-base (0.21-4) ...
Selecting previously unselected package libsigsegv2:amd64.
Preparing to unpack .../06-libsigsegv2_2.13-1_amd64.deb ...
Unpacking libsigsegv2:amd64 (2.13-1) ...
Selecting previously unselected package m4.
Preparing to unpack .../07-m4_1.4.18-5_amd64.deb ...
Unpacking m4 (1.4.18-5) ...
Selecting previously unselected package autoconf.
Preparing to unpack .../08-autoconf_2.69-14_all.deb ...
Unpacking autoconf (2.69-14) ...
Selecting previously unselected package autotools-dev.
Preparing to unpack .../09-autotools-dev_20180224.1+nmu1_all.deb ...
Unpacking autotools-dev (20180224.1+nmu1) ...
Selecting previously unselected package automake.
Preparing to unpack .../10-automake_1%3a1.16.3-2_all.deb ...
Unpacking automake (1:1.16.3-2) ...
Selecting previously unselected package autopoint.
Preparing to unpack .../11-autopoint_0.21-4_all.deb ...
Unpacking autopoint (0.21-4) ...
Selecting previously unselected package libdebhelper-perl.
Preparing to unpack .../12-libdebhelper-perl_13.3.4_all.deb ...
Unpacking libdebhelper-perl (13.3.4) ...
Selecting previously unselected package libtool.
Preparing to unpack .../13-libtool_2.4.6-15_all.deb ...
Unpacking libtool (2.4.6-15) ...
Selecting previously unselected package dh-autoreconf.
Preparing to unpack .../14-dh-autoreconf_20_all.deb ...
Unpacking dh-autoreconf (20) ...
Selecting previously unselected package libarchive-zip-perl.
Preparing to unpack .../15-libarchive-zip-perl_1.68-1_all.deb ...
Unpacking libarchive-zip-perl (1.68-1) ...
Selecting previously unselected package libsub-override-perl.
Preparing to unpack .../16-libsub-override-perl_0.09-2_all.deb ...
Unpacking libsub-override-perl (0.09-2) ...
Selecting previously unselected package libfile-stripnondeterminism-perl.
Preparing to unpack .../17-libfile-stripnondeterminism-perl_1.12.0-1_all.deb ...
Unpacking libfile-stripnondeterminism-perl (1.12.0-1) ...
Selecting previously unselected package dh-strip-nondeterminism.
Preparing to unpack .../18-dh-strip-nondeterminism_1.12.0-1_all.deb ...
Unpacking dh-strip-nondeterminism (1.12.0-1) ...
Selecting previously unselected package libelf1:amd64.
Preparing to unpack .../19-libelf1_0.183-1_amd64.deb ...
Unpacking libelf1:amd64 (0.183-1) ...
Selecting previously unselected package dwz.
Preparing to unpack .../20-dwz_0.13+20210201-1_amd64.deb ...
Unpacking dwz (0.13+20210201-1) ...
Selecting previously unselected package libicu67:amd64.
Preparing to unpack .../21-libicu67_67.1-7_amd64.deb ...
Unpacking libicu67:amd64 (67.1-7) ...
Selecting previously unselected package libxml2:amd64.
Preparing to unpack .../22-libxml2_2.9.10+dfsg-6.7+deb11u4_amd64.deb ...
Unpacking libxml2:amd64 (2.9.10+dfsg-6.7+deb11u4) ...
Selecting previously unselected package gettext.
Preparing to unpack .../23-gettext_0.21-4_amd64.deb ...
Unpacking gettext (0.21-4) ...
Selecting previously unselected package intltool-debian.
Preparing to unpack .../24-intltool-debian_0.35.0+20060710.5_all.deb ...
Unpacking intltool-debian (0.35.0+20060710.5) ...
Selecting previously unselected package po-debconf.
Preparing to unpack .../25-po-debconf_1.0.21+nmu1_all.deb ...
Unpacking po-debconf (1.0.21+nmu1) ...
Selecting previously unselected package debhelper.
Preparing to unpack .../26-debhelper_13.3.4_all.deb ...
Unpacking debhelper (13.3.4) ...
Setting up libpipeline1:amd64 (1.5.3-1) ...
Setting up bsdextrautils (2.36.1-8+deb11u2) ...
update-alternatives: using /usr/bin/write.ul to provide /usr/bin/write (write) in auto mode
update-alternatives: warning: skip creation of /usr/share/man/man1/write.1.gz because associated file /usr/share/man/man1/write.ul.1.gz (of link group write) doesn't exist
Setting up libicu67:amd64 (67.1-7) ...
Setting up libarchive-zip-perl (1.68-1) ...
Setting up libdebhelper-perl (13.3.4) ...
Setting up gettext-base (0.21-4) ...
Setting up autotools-dev (20180224.1+nmu1) ...
Setting up libsigsegv2:amd64 (2.13-1) ...
Setting up autopoint (0.21-4) ...
Setting up libuchardet0:amd64 (0.0.7-1) ...
Setting up libsub-override-perl (0.09-2) ...
Setting up libelf1:amd64 (0.183-1) ...
Setting up libxml2:amd64 (2.9.10+dfsg-6.7+deb11u4) ...
Setting up libfile-stripnondeterminism-perl (1.12.0-1) ...
Setting up gettext (0.21-4) ...
Setting up libtool (2.4.6-15) ...
Setting up m4 (1.4.18-5) ...
Setting up intltool-debian (0.35.0+20060710.5) ...
Setting up autoconf (2.69-14) ...
Setting up dh-strip-nondeterminism (1.12.0-1) ...
Setting up dwz (0.13+20210201-1) ...
Setting up groff-base (1.22.4-6) ...
Setting up automake (1:1.16.3-2) ...
update-alternatives: using /usr/bin/automake-1.16 to provide /usr/bin/automake (automake) in auto mode
update-alternatives: warning: skip creation of /usr/share/man/man1/automake.1.gz because associated file /usr/share/man/man1/automake-1.16.1.gz (of link group automake) doesn't exist
update-alternatives: warning: skip creation of /usr/share/man/man1/aclocal.1.gz because associated file /usr/share/man/man1/aclocal-1.16.1.gz (of link group automake) doesn't exist
Setting up po-debconf (1.0.21+nmu1) ...
Setting up man-db (2.9.4-2) ...
Building database of manual pages ...
Setting up dh-autoreconf (20) ...
Setting up debhelper (13.3.4) ...
Processing triggers for libc-bin (2.31-13+deb11u10) ...
+ env DEB_BUILD_OPTIONS=noautodbgsym nocheck eatmydata dpkg-buildpackage --no-sign
dpkg-buildpackage: info: source package popularity-contest
dpkg-buildpackage: info: source version 1.71
dpkg-buildpackage: info: source distribution unstable
dpkg-buildpackage: info: source changed by Bill Allombert <ballombe@debian.org>
 dpkg-source --before-build .
dpkg-buildpackage: info: host architecture amd64
 debian/rules clean
dh_testdir
dh_testroot
dh_clean
 dpkg-source -b .
dpkg-source: info: using source format '3.0 (native)'
dpkg-source: info: building popularity-contest in popularity-contest_1.71.tar.xz
dpkg-source: info: building popularity-contest in popularity-contest_1.71.dsc
 debian/rules build
dh_testdir
 debian/rules binary
dh_testdir
dh_testdir
dh_testroot
dh_prep
dh_installdirs
# Add here commands to install the package into debian/tmp.
install -d debian/popularity-contest/usr/sbin/
install -d debian/popularity-contest/etc/
install -d debian/popularity-contest/usr/share/popularity-contest/
install popularity-contest debian/popularity-contest/usr/sbin/
install popcon-upload debian/popularity-contest/usr/share/popularity-contest/
install popcon-largest-unused debian/popularity-contest/usr/sbin/
install -m 644 default.conf debian/popularity-contest/usr/share/popularity-contest/
install -m 644 debian-popcon.gpg debian/popularity-contest/usr/share/popularity-contest/
dh_testdir
dh_testroot
dh_installdocs README FAQ
dh_installcron
dh_installman debian/popularity-contest.8 debian/popcon-largest-unused.8
dh_installdebconf
dh_installchangelogs
dh_compress
dh_installexamples examples/bin examples/cgi-bin
dh_perl -d
dh_strip
dh_fixperms
dh_installdeb
dh_shlibdeps
dh_gencontrol
dpkg-gencontrol: warning: Depends field of package popularity-contest: substitution variable ${perl:Depends} used, but is not defined
dh_md5sums
dh_builddeb
dpkg-deb: building package 'popularity-contest' in '../popularity-contest_1.71_all.deb'.
 dpkg-genbuildinfo
 dpkg-genchanges  >../popularity-contest_1.71_amd64.changes
dpkg-genchanges: info: including full source code in upload
 dpkg-source --after-build .
dpkg-buildpackage: info: full upload; Debian-native package (full source is included)
+ date
Mon Jul  8 23:34:51 UTC 2024
+ cd ..
+ ls popularity-contest_1.71_all.deb+ sed
 -e s,_.*,,
+ apt-cache show popularity-contest=1.71
+ sed -e s,^Version: ,,
+ grep ^Version: 
+ binver=1.71
+ binnmuver=1.71
+ echo 1.71
+ sed s,^[0-9]\+:,,
+ binnmuver=1.71
+ echo 1.71
+ sed s,^[0-9]\+:,,
+ srcver=1.71
+ test -f popularity-contest_1.71_amd64.deb
+ ln -s popularity-contest_1.71_amd64.deb popularity-contest_1.71_amd64.deb
+ apt-cache show popularity-contest=1.71
+ sed -z s,.*Filename: .*/\(.*\)SHA256: \([0-9a-f]\{64\}\).*,\2  \1,g
+ grep -e ^Filename:  -e ^SHA256: 
+ ls -la
total 180
drwxr-xr-x 3 root root  4096 Jul  8 23:34 .
drwxr-xr-x 3 root root  4096 Jul  8 23:34 ..
-rw-r--r-- 1 root root    98 Jul  8 23:34 SHA256SUMS
drwxr-xr-x 4 root root  4096 Dec 12  2020 popularity-contest-1.71
-rw-r--r-- 1 root root   848 Jul  8 23:34 popularity-contest_1.71.dsc
-rw-r--r-- 1 root root 77868 Jul  8 23:34 popularity-contest_1.71.tar.xz
-rw-r--r-- 1 root root 68672 Jul  8 23:34 popularity-contest_1.71_all.deb
-rw-r--r-- 1 root root  5185 Jul  8 23:34 popularity-contest_1.71_amd64.buildinfo
-rw-r--r-- 1 root root  1840 Jul  8 23:34 popularity-contest_1.71_amd64.changes
lrwxrwxrwx 1 root root    33 Jul  8 23:34 popularity-contest_1.71_amd64.deb -> popularity-contest_1.71_amd64.deb
+ find . -maxdepth 1 -type f
+ sha256sum ./popularity-contest_1.71_amd64.buildinfo ./popularity-contest_1.71.tar.xz ./popularity-contest_1.71.dsc ./popularity-contest_1.71_amd64.changes ./popularity-contest_1.71_all.deb ./SHA256SUMS
d243dc3dd19c97a0eed6f5f5aa43d624e30241cfe126d00330f7f6f6aab51234  ./popularity-contest_1.71_amd64.buildinfo
ca77dcd5ac5ed66b1a3f5843fdc9c59295bfa902e1ef8b390d33cfac10ff0171  ./popularity-contest_1.71.tar.xz
6083756cd10b31dc99de67860f46dffbae56fbab0f86930d2921fec12b6abf22  ./popularity-contest_1.71.dsc
d81ba5433269d225e02de5a451bbab99a37140b47abb30b1d01b15e28791e500  ./popularity-contest_1.71_amd64.changes
da7f9e8a19fc33a8fa4624edb101f8154008687b02fc1ad93da4be7b76a7feb8  ./popularity-contest_1.71_all.deb
c689adb80e73032df783ccb81ddd1236c3581b86cb6d10cc619c1ff2bc7ee890  ./SHA256SUMS
+ tail -v -n+0 SHA256SUMS
==> SHA256SUMS <==
d076923476701d8e3f39cc52f14f972d6a17de5460f87ee0290ef03d39af820c  popularity-contest_1.71_all.deb
+ sha256sum -c SHA256SUMS
popularity-contest_1.71_all.deb: FAILED
sha256sum: WARNING: 1 computed checksum did NOT match
