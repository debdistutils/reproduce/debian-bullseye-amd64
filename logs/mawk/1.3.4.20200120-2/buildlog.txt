+ date
Mon Jul  8 23:31:25 UTC 2024
+ id
uid=0(root) gid=0(root) groups=0(root)
+ pwd
/build/mawk
+ apt-get source --only-source mawk=1.3.4.20200120-2
Reading package lists...
NOTICE: 'mawk' packaging is maintained in the 'Git' version control system at:
https://salsa.debian.org/debian/mawk.git
Please use:
git clone https://salsa.debian.org/debian/mawk.git
to retrieve the latest (possibly unreleased) updates to the package.
Need to get 478 kB of source archives.
Get:1 http://deb.debian.org/debian bullseye/main mawk 1.3.4.20200120-2 (dsc) [1915 B]
Get:2 http://deb.debian.org/debian bullseye/main mawk 1.3.4.20200120-2 (tar) [469 kB]
Get:3 http://deb.debian.org/debian bullseye/main mawk 1.3.4.20200120-2 (diff) [7504 B]
dpkg-source: info: extracting mawk in mawk-1.3.4.20200120
dpkg-source: info: unpacking mawk_1.3.4.20200120.orig.tar.gz
dpkg-source: info: unpacking mawk_1.3.4.20200120-2.debian.tar.xz
Fetched 478 kB in 0s (1541 kB/s)
W: Download is performed unsandboxed as root as file 'mawk_1.3.4.20200120-2.dsc' couldn't be accessed by user '_apt'. - pkgAcquire::Run (13: Permission denied)
+ ls -la
total 484
drwxr-xr-x  3 root root   4096 Jul  8 23:31 .
drwxr-xr-x  3 root root   4096 Jul  8 23:31 ..
drwxr-xr-x 10 root root   4096 Jul  8 23:31 mawk-1.3.4.20200120
-rw-r--r--  1 root root   7504 Feb 16  2020 mawk_1.3.4.20200120-2.debian.tar.xz
-rw-r--r--  1 root root   1915 Feb 16  2020 mawk_1.3.4.20200120-2.dsc
-rw-r--r--  1 root root 468855 Jan 25  2020 mawk_1.3.4.20200120.orig.tar.gz
+ find . -maxdepth 1 -type f
+ sha256sum ./mawk_1.3.4.20200120-2.dsc ./mawk_1.3.4.20200120-2.debian.tar.xz ./mawk_1.3.4.20200120.orig.tar.gz
5069c46872ac74f5221250dfb88b31b1f2dbb8a2617c1e013f8f80cc34638c6d  ./mawk_1.3.4.20200120-2.dsc
b772ed2f016b0286980c46cbc1f1f4ae62887ef2aa3dff6ef10cae638f923f26  ./mawk_1.3.4.20200120-2.debian.tar.xz
7fd4cd1e1fae9290fe089171181bbc6291dfd9bca939ca804f0ddb851c8b8237  ./mawk_1.3.4.20200120.orig.tar.gz
+ find . -maxdepth 1 -name mawk* -type d
+ cd ./mawk-1.3.4.20200120
+ env DEBIAN_FRONTEND=noninteractive apt-get build-dep -y --only-source mawk=1.3.4.20200120-2
Reading package lists...
Building dependency tree...
Reading state information...
The following NEW packages will be installed:
  autoconf autoconf-dickey automake autopoint autotools-dev bison
  bsdextrautils debhelper dh-autoreconf dh-strip-nondeterminism dwz gettext
  gettext-base groff-base intltool-debian libarchive-zip-perl
  libdebhelper-perl libelf1 libfile-stripnondeterminism-perl libicu67
  libpipeline1 libsigsegv2 libsub-override-perl libtool libuchardet0 libxml2
  m4 man-db po-debconf
0 upgraded, 29 newly installed, 0 to remove and 0 not upgraded.
Need to get 19.1 MB of archives.
After this operation, 64.4 MB of additional disk space will be used.
Get:1 http://deb.debian.org/debian bullseye/main amd64 bsdextrautils amd64 2.36.1-8+deb11u2 [146 kB]
Get:2 http://deb.debian.org/debian bullseye/main amd64 libuchardet0 amd64 0.0.7-1 [67.8 kB]
Get:3 http://deb.debian.org/debian bullseye/main amd64 groff-base amd64 1.22.4-6 [936 kB]
Get:4 http://deb.debian.org/debian bullseye/main amd64 libpipeline1 amd64 1.5.3-1 [34.3 kB]
Get:5 http://deb.debian.org/debian bullseye/main amd64 man-db amd64 2.9.4-2 [1354 kB]
Get:6 http://deb.debian.org/debian bullseye/main amd64 gettext-base amd64 0.21-4 [175 kB]
Get:7 http://deb.debian.org/debian bullseye/main amd64 libsigsegv2 amd64 2.13-1 [34.8 kB]
Get:8 http://deb.debian.org/debian bullseye/main amd64 m4 amd64 1.4.18-5 [204 kB]
Get:9 http://deb.debian.org/debian bullseye/main amd64 autoconf all 2.69-14 [313 kB]
Get:10 http://deb.debian.org/debian bullseye/main amd64 autoconf-dickey all 2.52+20210105-1 [163 kB]
Get:11 http://deb.debian.org/debian bullseye/main amd64 autotools-dev all 20180224.1+nmu1 [77.1 kB]
Get:12 http://deb.debian.org/debian bullseye/main amd64 automake all 1:1.16.3-2 [814 kB]
Get:13 http://deb.debian.org/debian bullseye/main amd64 autopoint all 0.21-4 [510 kB]
Get:14 http://deb.debian.org/debian bullseye/main amd64 bison amd64 2:3.7.5+dfsg-1 [1104 kB]
Get:15 http://deb.debian.org/debian bullseye/main amd64 libdebhelper-perl all 13.3.4 [189 kB]
Get:16 http://deb.debian.org/debian bullseye/main amd64 libtool all 2.4.6-15 [513 kB]
Get:17 http://deb.debian.org/debian bullseye/main amd64 dh-autoreconf all 20 [17.1 kB]
Get:18 http://deb.debian.org/debian bullseye/main amd64 libarchive-zip-perl all 1.68-1 [104 kB]
Get:19 http://deb.debian.org/debian bullseye/main amd64 libsub-override-perl all 0.09-2 [10.2 kB]
Get:20 http://deb.debian.org/debian bullseye/main amd64 libfile-stripnondeterminism-perl all 1.12.0-1 [26.3 kB]
Get:21 http://deb.debian.org/debian bullseye/main amd64 dh-strip-nondeterminism all 1.12.0-1 [15.4 kB]
Get:22 http://deb.debian.org/debian bullseye/main amd64 libelf1 amd64 0.183-1 [165 kB]
Get:23 http://deb.debian.org/debian bullseye/main amd64 dwz amd64 0.13+20210201-1 [175 kB]
Get:24 http://deb.debian.org/debian bullseye/main amd64 libicu67 amd64 67.1-7 [8622 kB]
Get:25 http://deb.debian.org/debian bullseye/main amd64 libxml2 amd64 2.9.10+dfsg-6.7+deb11u4 [693 kB]
Get:26 http://deb.debian.org/debian bullseye/main amd64 gettext amd64 0.21-4 [1311 kB]
Get:27 http://deb.debian.org/debian bullseye/main amd64 intltool-debian all 0.35.0+20060710.5 [26.8 kB]
Get:28 http://deb.debian.org/debian bullseye/main amd64 po-debconf all 1.0.21+nmu1 [248 kB]
Get:29 http://deb.debian.org/debian bullseye/main amd64 debhelper all 13.3.4 [1049 kB]
debconf: delaying package configuration, since apt-utils is not installed
Fetched 19.1 MB in 1s (34.6 MB/s)
Selecting previously unselected package bsdextrautils.
(Reading database ... (Reading database ... 5%(Reading database ... 10%(Reading database ... 15%(Reading database ... 20%(Reading database ... 25%(Reading database ... 30%(Reading database ... 35%(Reading database ... 40%(Reading database ... 45%(Reading database ... 50%(Reading database ... 55%(Reading database ... 60%(Reading database ... 65%(Reading database ... 70%(Reading database ... 75%(Reading database ... 80%(Reading database ... 85%(Reading database ... 90%(Reading database ... 95%(Reading database ... 100%(Reading database ... 16217 files and directories currently installed.)
Preparing to unpack .../00-bsdextrautils_2.36.1-8+deb11u2_amd64.deb ...
Unpacking bsdextrautils (2.36.1-8+deb11u2) ...
Selecting previously unselected package libuchardet0:amd64.
Preparing to unpack .../01-libuchardet0_0.0.7-1_amd64.deb ...
Unpacking libuchardet0:amd64 (0.0.7-1) ...
Selecting previously unselected package groff-base.
Preparing to unpack .../02-groff-base_1.22.4-6_amd64.deb ...
Unpacking groff-base (1.22.4-6) ...
Selecting previously unselected package libpipeline1:amd64.
Preparing to unpack .../03-libpipeline1_1.5.3-1_amd64.deb ...
Unpacking libpipeline1:amd64 (1.5.3-1) ...
Selecting previously unselected package man-db.
Preparing to unpack .../04-man-db_2.9.4-2_amd64.deb ...
Unpacking man-db (2.9.4-2) ...
Selecting previously unselected package gettext-base.
Preparing to unpack .../05-gettext-base_0.21-4_amd64.deb ...
Unpacking gettext-base (0.21-4) ...
Selecting previously unselected package libsigsegv2:amd64.
Preparing to unpack .../06-libsigsegv2_2.13-1_amd64.deb ...
Unpacking libsigsegv2:amd64 (2.13-1) ...
Selecting previously unselected package m4.
Preparing to unpack .../07-m4_1.4.18-5_amd64.deb ...
Unpacking m4 (1.4.18-5) ...
Selecting previously unselected package autoconf.
Preparing to unpack .../08-autoconf_2.69-14_all.deb ...
Unpacking autoconf (2.69-14) ...
Selecting previously unselected package autoconf-dickey.
Preparing to unpack .../09-autoconf-dickey_2.52+20210105-1_all.deb ...
Unpacking autoconf-dickey (2.52+20210105-1) ...
Selecting previously unselected package autotools-dev.
Preparing to unpack .../10-autotools-dev_20180224.1+nmu1_all.deb ...
Unpacking autotools-dev (20180224.1+nmu1) ...
Selecting previously unselected package automake.
Preparing to unpack .../11-automake_1%3a1.16.3-2_all.deb ...
Unpacking automake (1:1.16.3-2) ...
Selecting previously unselected package autopoint.
Preparing to unpack .../12-autopoint_0.21-4_all.deb ...
Unpacking autopoint (0.21-4) ...
Selecting previously unselected package bison.
Preparing to unpack .../13-bison_2%3a3.7.5+dfsg-1_amd64.deb ...
Unpacking bison (2:3.7.5+dfsg-1) ...
Selecting previously unselected package libdebhelper-perl.
Preparing to unpack .../14-libdebhelper-perl_13.3.4_all.deb ...
Unpacking libdebhelper-perl (13.3.4) ...
Selecting previously unselected package libtool.
Preparing to unpack .../15-libtool_2.4.6-15_all.deb ...
Unpacking libtool (2.4.6-15) ...
Selecting previously unselected package dh-autoreconf.
Preparing to unpack .../16-dh-autoreconf_20_all.deb ...
Unpacking dh-autoreconf (20) ...
Selecting previously unselected package libarchive-zip-perl.
Preparing to unpack .../17-libarchive-zip-perl_1.68-1_all.deb ...
Unpacking libarchive-zip-perl (1.68-1) ...
Selecting previously unselected package libsub-override-perl.
Preparing to unpack .../18-libsub-override-perl_0.09-2_all.deb ...
Unpacking libsub-override-perl (0.09-2) ...
Selecting previously unselected package libfile-stripnondeterminism-perl.
Preparing to unpack .../19-libfile-stripnondeterminism-perl_1.12.0-1_all.deb ...
Unpacking libfile-stripnondeterminism-perl (1.12.0-1) ...
Selecting previously unselected package dh-strip-nondeterminism.
Preparing to unpack .../20-dh-strip-nondeterminism_1.12.0-1_all.deb ...
Unpacking dh-strip-nondeterminism (1.12.0-1) ...
Selecting previously unselected package libelf1:amd64.
Preparing to unpack .../21-libelf1_0.183-1_amd64.deb ...
Unpacking libelf1:amd64 (0.183-1) ...
Selecting previously unselected package dwz.
Preparing to unpack .../22-dwz_0.13+20210201-1_amd64.deb ...
Unpacking dwz (0.13+20210201-1) ...
Selecting previously unselected package libicu67:amd64.
Preparing to unpack .../23-libicu67_67.1-7_amd64.deb ...
Unpacking libicu67:amd64 (67.1-7) ...
Selecting previously unselected package libxml2:amd64.
Preparing to unpack .../24-libxml2_2.9.10+dfsg-6.7+deb11u4_amd64.deb ...
Unpacking libxml2:amd64 (2.9.10+dfsg-6.7+deb11u4) ...
Selecting previously unselected package gettext.
Preparing to unpack .../25-gettext_0.21-4_amd64.deb ...
Unpacking gettext (0.21-4) ...
Selecting previously unselected package intltool-debian.
Preparing to unpack .../26-intltool-debian_0.35.0+20060710.5_all.deb ...
Unpacking intltool-debian (0.35.0+20060710.5) ...
Selecting previously unselected package po-debconf.
Preparing to unpack .../27-po-debconf_1.0.21+nmu1_all.deb ...
Unpacking po-debconf (1.0.21+nmu1) ...
Selecting previously unselected package debhelper.
Preparing to unpack .../28-debhelper_13.3.4_all.deb ...
Unpacking debhelper (13.3.4) ...
Setting up libpipeline1:amd64 (1.5.3-1) ...
Setting up bsdextrautils (2.36.1-8+deb11u2) ...
update-alternatives: using /usr/bin/write.ul to provide /usr/bin/write (write) in auto mode
update-alternatives: warning: skip creation of /usr/share/man/man1/write.1.gz because associated file /usr/share/man/man1/write.ul.1.gz (of link group write) doesn't exist
Setting up libicu67:amd64 (67.1-7) ...
Setting up libarchive-zip-perl (1.68-1) ...
Setting up libdebhelper-perl (13.3.4) ...
Setting up gettext-base (0.21-4) ...
Setting up autotools-dev (20180224.1+nmu1) ...
Setting up libsigsegv2:amd64 (2.13-1) ...
Setting up autopoint (0.21-4) ...
Setting up libuchardet0:amd64 (0.0.7-1) ...
Setting up libsub-override-perl (0.09-2) ...
Setting up libelf1:amd64 (0.183-1) ...
Setting up libxml2:amd64 (2.9.10+dfsg-6.7+deb11u4) ...
Setting up libfile-stripnondeterminism-perl (1.12.0-1) ...
Setting up gettext (0.21-4) ...
Setting up libtool (2.4.6-15) ...
Setting up m4 (1.4.18-5) ...
Setting up intltool-debian (0.35.0+20060710.5) ...
Setting up autoconf (2.69-14) ...
Setting up dh-strip-nondeterminism (1.12.0-1) ...
Setting up dwz (0.13+20210201-1) ...
Setting up groff-base (1.22.4-6) ...
Setting up bison (2:3.7.5+dfsg-1) ...
update-alternatives: using /usr/bin/bison.yacc to provide /usr/bin/yacc (yacc) in auto mode
update-alternatives: warning: skip creation of /usr/share/man/man1/yacc.1.gz because associated file /usr/share/man/man1/bison.yacc.1.gz (of link group yacc) doesn't exist
Setting up automake (1:1.16.3-2) ...
update-alternatives: using /usr/bin/automake-1.16 to provide /usr/bin/automake (automake) in auto mode
update-alternatives: warning: skip creation of /usr/share/man/man1/automake.1.gz because associated file /usr/share/man/man1/automake-1.16.1.gz (of link group automake) doesn't exist
update-alternatives: warning: skip creation of /usr/share/man/man1/aclocal.1.gz because associated file /usr/share/man/man1/aclocal-1.16.1.gz (of link group automake) doesn't exist
Setting up po-debconf (1.0.21+nmu1) ...
Setting up autoconf-dickey (2.52+20210105-1) ...
Setting up man-db (2.9.4-2) ...
Building database of manual pages ...
Setting up dh-autoreconf (20) ...
Setting up debhelper (13.3.4) ...
Processing triggers for libc-bin (2.31-13+deb11u10) ...
+ env DEB_BUILD_OPTIONS=noautodbgsym nocheck eatmydata dpkg-buildpackage --no-sign
dpkg-buildpackage: info: source package mawk
dpkg-buildpackage: info: source version 1.3.4.20200120-2
dpkg-buildpackage: info: source distribution unstable
dpkg-buildpackage: info: source changed by Boyuan Yang <byang@debian.org>
 dpkg-source --before-build .
dpkg-buildpackage: info: host architecture amd64
 debian/rules clean
dh clean --no-parallel
   dh_auto_clean -O--no-parallel
   dh_autoreconf_clean -O--no-parallel
   dh_clean -O--no-parallel
 dpkg-source -b .
dpkg-source: info: using source format '3.0 (quilt)'
dpkg-source: info: building mawk using existing ./mawk_1.3.4.20200120.orig.tar.gz
dpkg-source: warning: ignoring deletion of file parse.c, use --include-removal to override
dpkg-source: warning: ignoring deletion of file parse.h, use --include-removal to override
dpkg-source: info: building mawk in mawk_1.3.4.20200120-2.debian.tar.xz
dpkg-source: info: building mawk in mawk_1.3.4.20200120-2.dsc
 debian/rules build
dh build --no-parallel
   dh_update_autotools_config -O--no-parallel
   debian/rules override_dh_autoreconf
make[1]: Entering directory '/build/mawk/mawk-1.3.4.20200120'
dh_autoreconf autoreconf-dickey -- -f -i
make[1]: Leaving directory '/build/mawk/mawk-1.3.4.20200120'
   dh_auto_configure -O--no-parallel
	./configure --build=x86_64-linux-gnu --prefix=/usr --includedir=\${prefix}/include --mandir=\${prefix}/share/man --infodir=\${prefix}/share/info --sysconfdir=/etc --localstatedir=/var --disable-option-checking --disable-silent-rules --libdir=\${prefix}/lib/x86_64-linux-gnu --runstatedir=/run --disable-maintainer-mode --disable-dependency-tracking
checking build system type... x86_64-pc-linux-gnu
checking host system type... x86_64-pc-linux-gnu
Configuring for linux-gnu
checking for gcc... gcc
checking for C compiler default output... a.out
checking whether the C compiler works... yes
checking whether we are cross compiling... no
checking for executable suffix... 
checking for object suffix... o
checking whether we are using the GNU C compiler... yes
checking whether gcc accepts -g... yes
checking version of gcc... 10.2.1
checking if this is really Intel C compiler... no
checking if this is really Clang C compiler... no
checking for gcc option to accept ANSI C... none needed
checking $CFLAGS variable... ok
checking $CC variable... ok
checking whether make sets ${MAKE}... yes
checking for makeflags variable... -${MAKEFLAGS}
checking for ggrep... no
checking for grep... grep
checking for egrep... grep -E
checking how to run the C preprocessor... gcc -E
checking for a BSD compatible install... /usr/bin/install -c
checking for log in -lm... yes
checking if you want to see long compiling messages... yes
checking if you want to turn on gcc warnings... no
checking for groff... /usr/bin/groff
checking for nroff... /usr/bin/nroff
checking for tbl... /usr/bin/tbl
checking for man2html... no
checking for program to convert manpage to html... /usr/bin/groff
checking if you want to use mawk's own regular-expressions engine... yes
checking if you want to use mawk's own srand/rand functions... no
checking for random-integer functions... srandom/random
checking for range of random-integers... (1UL<<31)-1
checking if you want mawk to initialize random numbers at startup... yes
checking for byacc... no
checking for bison... bison -y
checking for lint... no
checking for cppcheck... no
checking for splint... no
checking if filesystem supports mixed-case filenames... yes
checking for exctags... no
checking for ctags... no
checking for exetags... no
checking for etags... no
checking for ctags... no
checking for etags... no
checking if the POSIX test-macros are already defined... no
checking if this is the GNU C library... yes
checking if _DEFAULT_SOURCE can be used as a basis... yes
checking if _XOPEN_SOURCE=500 works with _DEFAULT_SOURCE... yes
checking if _XOPEN_SOURCE really is set... yes
checking for special C compiler options needed for large files... no
checking for _FILE_OFFSET_BITS value needed for large files... no
checking for _LARGE_FILES value needed for large files... no
checking for _LARGEFILE_SOURCE value needed for large files... no
checking for fseeko... yes
checking whether to use struct dirent64... no
checking if you want to use dmalloc for testing... no
checking if you want to use dbmalloc for testing... no
checking if you want to use valgrind for testing... no
checking if you want to perform memory-leak testing... no
checking if you want to enable debugging trace... no
checking for stddef.h... yes
checking if size_t is declared in stddef.h... yes
checking for setlocale()... yes
checking if external environ is declared... no
checking if external environ exists... yes
checking for fork... yes
checking for gettimeofday... yes
checking for matherr... no
checking for mktime... yes
checking for pipe... yes
checking for strftime... yes
checking for tdestroy... yes
checking for tsearch... yes
checking for wait... yes
checking for errno.h... yes
checking for fcntl.h... yes
checking for unistd.h... yes
checking for sys/wait.h... yes
checking if math.h declares _LIB_VERSION... no
checking for limits.h... yes
checking for isnan... yes
checking for sigaction... yes
checking for siginfo.h... no
checking if we should use siginfo... no
checking return type of signal handlers... void
checking if we should use sigaction.sa_sigaction... yes
checking handling of floating point exceptions
	division by zero does not generate an exception
	overflow does not generate an exception
	math library supports ieee754
configure: creating ./config.status
config.status: creating Makefile
config.status: creating man/Makefile
config.status: creating config.h
   debian/rules override_dh_auto_build
make[1]: Entering directory '/build/mawk/mawk-1.3.4.20200120'
dh_auto_build -- mawk
	make -j1 mawk
make[2]: Entering directory '/build/mawk/mawk-1.3.4.20200120'
expect 4 shift/reduce conflicts
bison -y -d parse.y
parse.y: warning: 4 shift/reduce conflicts [-Wconflicts-sr]
parse.y: note: rerun with option '-Wcounterexamples' to generate conflict counterexamples
sed -e '/^#line/s%"y.tab.c"%"parse.c"%' y.tab.c >parse.c
rm -f y.tab.c
if cmp -s y.tab.h parse.h ;\
   then rm y.tab.h ;\
   else mv y.tab.h parse.h ; fi
gcc -c -I. -I. -DHAVE_CONFIG_H -Wdate-time -D_FORTIFY_SOURCE=2 -DLOCAL_REGEXP -D_DEFAULT_SOURCE -D_XOPEN_SOURCE=500 -g -O2 -ffile-prefix-map=/build/mawk/mawk-1.3.4.20200120=. -fstack-protector-strong -Wformat -Werror=format-security  parse.c
gcc -c -I. -I. -DHAVE_CONFIG_H -Wdate-time -D_FORTIFY_SOURCE=2 -DLOCAL_REGEXP -D_DEFAULT_SOURCE -D_XOPEN_SOURCE=500 -g -O2 -ffile-prefix-map=/build/mawk/mawk-1.3.4.20200120=. -fstack-protector-strong -Wformat -Werror=format-security  scan.c
gcc -c -I. -I. -DHAVE_CONFIG_H -Wdate-time -D_FORTIFY_SOURCE=2 -DLOCAL_REGEXP -D_DEFAULT_SOURCE -D_XOPEN_SOURCE=500 -g -O2 -ffile-prefix-map=/build/mawk/mawk-1.3.4.20200120=. -fstack-protector-strong -Wformat -Werror=format-security  memory.c
gcc -c -I. -I. -DHAVE_CONFIG_H -Wdate-time -D_FORTIFY_SOURCE=2 -DLOCAL_REGEXP -D_DEFAULT_SOURCE -D_XOPEN_SOURCE=500 -g -O2 -ffile-prefix-map=/build/mawk/mawk-1.3.4.20200120=. -fstack-protector-strong -Wformat -Werror=format-security  main.c
gcc -c -I. -I. -DHAVE_CONFIG_H -Wdate-time -D_FORTIFY_SOURCE=2 -DLOCAL_REGEXP -D_DEFAULT_SOURCE -D_XOPEN_SOURCE=500 -g -O2 -ffile-prefix-map=/build/mawk/mawk-1.3.4.20200120=. -fstack-protector-strong -Wformat -Werror=format-security  hash.c
gcc -c -I. -I. -DHAVE_CONFIG_H -Wdate-time -D_FORTIFY_SOURCE=2 -DLOCAL_REGEXP -D_DEFAULT_SOURCE -D_XOPEN_SOURCE=500 -g -O2 -ffile-prefix-map=/build/mawk/mawk-1.3.4.20200120=. -fstack-protector-strong -Wformat -Werror=format-security  execute.c
gcc -c -I. -I. -DHAVE_CONFIG_H -Wdate-time -D_FORTIFY_SOURCE=2 -DLOCAL_REGEXP -D_DEFAULT_SOURCE -D_XOPEN_SOURCE=500 -g -O2 -ffile-prefix-map=/build/mawk/mawk-1.3.4.20200120=. -fstack-protector-strong -Wformat -Werror=format-security  code.c
gcc -c -I. -I. -DHAVE_CONFIG_H -Wdate-time -D_FORTIFY_SOURCE=2 -DLOCAL_REGEXP -D_DEFAULT_SOURCE -D_XOPEN_SOURCE=500 -g -O2 -ffile-prefix-map=/build/mawk/mawk-1.3.4.20200120=. -fstack-protector-strong -Wformat -Werror=format-security  da.c
gcc -c -I. -I. -DHAVE_CONFIG_H -Wdate-time -D_FORTIFY_SOURCE=2 -DLOCAL_REGEXP -D_DEFAULT_SOURCE -D_XOPEN_SOURCE=500 -g -O2 -ffile-prefix-map=/build/mawk/mawk-1.3.4.20200120=. -fstack-protector-strong -Wformat -Werror=format-security  error.c
gcc -c -I. -I. -DHAVE_CONFIG_H -Wdate-time -D_FORTIFY_SOURCE=2 -DLOCAL_REGEXP -D_DEFAULT_SOURCE -D_XOPEN_SOURCE=500 -g -O2 -ffile-prefix-map=/build/mawk/mawk-1.3.4.20200120=. -fstack-protector-strong -Wformat -Werror=format-security  init.c
gcc -c -I. -I. -DHAVE_CONFIG_H -Wdate-time -D_FORTIFY_SOURCE=2 -DLOCAL_REGEXP -D_DEFAULT_SOURCE -D_XOPEN_SOURCE=500 -g -O2 -ffile-prefix-map=/build/mawk/mawk-1.3.4.20200120=. -fstack-protector-strong -Wformat -Werror=format-security  bi_vars.c
gcc -c -I. -I. -DHAVE_CONFIG_H -Wdate-time -D_FORTIFY_SOURCE=2 -DLOCAL_REGEXP -D_DEFAULT_SOURCE -D_XOPEN_SOURCE=500 -g -O2 -ffile-prefix-map=/build/mawk/mawk-1.3.4.20200120=. -fstack-protector-strong -Wformat -Werror=format-security  cast.c
gcc -c -I. -I. -DHAVE_CONFIG_H -Wdate-time -D_FORTIFY_SOURCE=2 -DLOCAL_REGEXP -D_DEFAULT_SOURCE -D_XOPEN_SOURCE=500 -g -O2 -ffile-prefix-map=/build/mawk/mawk-1.3.4.20200120=. -fstack-protector-strong -Wformat -Werror=format-security  print.c
gcc -c -I. -I. -DHAVE_CONFIG_H -Wdate-time -D_FORTIFY_SOURCE=2 -DLOCAL_REGEXP -D_DEFAULT_SOURCE -D_XOPEN_SOURCE=500 -g -O2 -ffile-prefix-map=/build/mawk/mawk-1.3.4.20200120=. -fstack-protector-strong -Wformat -Werror=format-security  bi_funct.c
gcc -c -I. -I. -DHAVE_CONFIG_H -Wdate-time -D_FORTIFY_SOURCE=2 -DLOCAL_REGEXP -D_DEFAULT_SOURCE -D_XOPEN_SOURCE=500 -g -O2 -ffile-prefix-map=/build/mawk/mawk-1.3.4.20200120=. -fstack-protector-strong -Wformat -Werror=format-security  kw.c
gcc -c -I. -I. -DHAVE_CONFIG_H -Wdate-time -D_FORTIFY_SOURCE=2 -DLOCAL_REGEXP -D_DEFAULT_SOURCE -D_XOPEN_SOURCE=500 -g -O2 -ffile-prefix-map=/build/mawk/mawk-1.3.4.20200120=. -fstack-protector-strong -Wformat -Werror=format-security  jmp.c
gcc -c -I. -I. -DHAVE_CONFIG_H -Wdate-time -D_FORTIFY_SOURCE=2 -DLOCAL_REGEXP -D_DEFAULT_SOURCE -D_XOPEN_SOURCE=500 -g -O2 -ffile-prefix-map=/build/mawk/mawk-1.3.4.20200120=. -fstack-protector-strong -Wformat -Werror=format-security  array.c
gcc -c -I. -I. -DHAVE_CONFIG_H -Wdate-time -D_FORTIFY_SOURCE=2 -DLOCAL_REGEXP -D_DEFAULT_SOURCE -D_XOPEN_SOURCE=500 -g -O2 -ffile-prefix-map=/build/mawk/mawk-1.3.4.20200120=. -fstack-protector-strong -Wformat -Werror=format-security  field.c
gcc -c -I. -I. -DHAVE_CONFIG_H -Wdate-time -D_FORTIFY_SOURCE=2 -DLOCAL_REGEXP -D_DEFAULT_SOURCE -D_XOPEN_SOURCE=500 -g -O2 -ffile-prefix-map=/build/mawk/mawk-1.3.4.20200120=. -fstack-protector-strong -Wformat -Werror=format-security  split.c
gcc -c -I. -I. -DHAVE_CONFIG_H -Wdate-time -D_FORTIFY_SOURCE=2 -DLOCAL_REGEXP -D_DEFAULT_SOURCE -D_XOPEN_SOURCE=500 -g -O2 -ffile-prefix-map=/build/mawk/mawk-1.3.4.20200120=. -fstack-protector-strong -Wformat -Werror=format-security  re_cmpl.c
gcc -c -I. -I. -DHAVE_CONFIG_H -Wdate-time -D_FORTIFY_SOURCE=2 -DLOCAL_REGEXP -D_DEFAULT_SOURCE -D_XOPEN_SOURCE=500 -g -O2 -ffile-prefix-map=/build/mawk/mawk-1.3.4.20200120=. -fstack-protector-strong -Wformat -Werror=format-security  regexp.c
gcc -c -I. -I. -DHAVE_CONFIG_H -Wdate-time -D_FORTIFY_SOURCE=2 -DLOCAL_REGEXP -D_DEFAULT_SOURCE -D_XOPEN_SOURCE=500 -g -O2 -ffile-prefix-map=/build/mawk/mawk-1.3.4.20200120=. -fstack-protector-strong -Wformat -Werror=format-security  zmalloc.c
gcc -c -I. -I. -DHAVE_CONFIG_H -Wdate-time -D_FORTIFY_SOURCE=2 -DLOCAL_REGEXP -D_DEFAULT_SOURCE -D_XOPEN_SOURCE=500 -g -O2 -ffile-prefix-map=/build/mawk/mawk-1.3.4.20200120=. -fstack-protector-strong -Wformat -Werror=format-security  fin.c
gcc -c -I. -I. -DHAVE_CONFIG_H -Wdate-time -D_FORTIFY_SOURCE=2 -DLOCAL_REGEXP -D_DEFAULT_SOURCE -D_XOPEN_SOURCE=500 -g -O2 -ffile-prefix-map=/build/mawk/mawk-1.3.4.20200120=. -fstack-protector-strong -Wformat -Werror=format-security  files.c
gcc -g -O2 -ffile-prefix-map=/build/mawk/mawk-1.3.4.20200120=. -fstack-protector-strong -Wformat -Werror=format-security  -I. -I. -DHAVE_CONFIG_H -I. -I. -DHAVE_CONFIG_H -Wdate-time -D_FORTIFY_SOURCE=2 -DLOCAL_REGEXP -D_DEFAULT_SOURCE -D_XOPEN_SOURCE=500 -g -O2 -ffile-prefix-map=/build/mawk/mawk-1.3.4.20200120=. -fstack-protector-strong -Wformat -Werror=format-security -Wl,-z,relro -Wl,-z,now -Wl,--as-needed -o makescan ./makescan.c
rm -f scancode.c
./makescan > scancode.c
rm makescan
gcc -c -I. -I. -DHAVE_CONFIG_H -Wdate-time -D_FORTIFY_SOURCE=2 -DLOCAL_REGEXP -D_DEFAULT_SOURCE -D_XOPEN_SOURCE=500 -g -O2 -ffile-prefix-map=/build/mawk/mawk-1.3.4.20200120=. -fstack-protector-strong -Wformat -Werror=format-security  scancode.c
gcc -c -I. -I. -DHAVE_CONFIG_H -Wdate-time -D_FORTIFY_SOURCE=2 -DLOCAL_REGEXP -D_DEFAULT_SOURCE -D_XOPEN_SOURCE=500 -g -O2 -ffile-prefix-map=/build/mawk/mawk-1.3.4.20200120=. -fstack-protector-strong -Wformat -Werror=format-security  matherr.c
gcc -c -I. -I. -DHAVE_CONFIG_H -Wdate-time -D_FORTIFY_SOURCE=2 -DLOCAL_REGEXP -D_DEFAULT_SOURCE -D_XOPEN_SOURCE=500 -g -O2 -ffile-prefix-map=/build/mawk/mawk-1.3.4.20200120=. -fstack-protector-strong -Wformat -Werror=format-security  fcall.c
gcc -c -I. -I. -DHAVE_CONFIG_H -Wdate-time -D_FORTIFY_SOURCE=2 -DLOCAL_REGEXP -D_DEFAULT_SOURCE -D_XOPEN_SOURCE=500 -g -O2 -ffile-prefix-map=/build/mawk/mawk-1.3.4.20200120=. -fstack-protector-strong -Wformat -Werror=format-security  version.c
gcc -g -O2 -ffile-prefix-map=/build/mawk/mawk-1.3.4.20200120=. -fstack-protector-strong -Wformat -Werror=format-security  -g -O2 -ffile-prefix-map=/build/mawk/mawk-1.3.4.20200120=. -fstack-protector-strong -Wformat -Werror=format-security -Wl,-z,relro -Wl,-z,now -Wl,--as-needed -o mawk parse.o scan.o memory.o main.o hash.o execute.o code.o da.o error.o init.o bi_vars.o cast.o print.o bi_funct.o kw.o jmp.o array.o field.o split.o re_cmpl.o regexp.o zmalloc.o fin.o files.o scancode.o matherr.o fcall.o version.o  -lm 
make[2]: Leaving directory '/build/mawk/mawk-1.3.4.20200120'
make[1]: Leaving directory '/build/mawk/mawk-1.3.4.20200120'
   create-stamp debian/debhelper-build-stamp
 debian/rules binary
dh binary --no-parallel
   dh_testroot -O--no-parallel
   dh_prep -O--no-parallel
   dh_installdirs -O--no-parallel
   debian/rules override_dh_auto_install
make[1]: Entering directory '/build/mawk/mawk-1.3.4.20200120'
dh_auto_install -- BINDIR=/build/mawk/mawk-1.3.4.20200120/debian/mawk/usr/bin \
                   MANDIR=/build/mawk/mawk-1.3.4.20200120/debian/mawk/usr/share/man/man1
	make -j1 install DESTDIR=/build/mawk/mawk-1.3.4.20200120/debian/mawk AM_UPDATE_INFO_DIR=no BINDIR=/build/mawk/mawk-1.3.4.20200120/debian/mawk/usr/bin MANDIR=/build/mawk/mawk-1.3.4.20200120/debian/mawk/usr/share/man/man1
make[2]: Entering directory '/build/mawk/mawk-1.3.4.20200120'
/usr/bin/install -c mawk /build/mawk/mawk-1.3.4.20200120/debian/mawk/usr/bin/`echo mawk|    sed 's,x,x,'`
/usr/bin/install -c -m 644 ./man/mawk.1 /build/mawk/mawk-1.3.4.20200120/debian/mawk/usr/share/man/man1/`echo mawk|    sed 's,x,x,'`.1
make[2]: Leaving directory '/build/mawk/mawk-1.3.4.20200120'
make[1]: Leaving directory '/build/mawk/mawk-1.3.4.20200120'
   dh_installdocs -O--no-parallel
   dh_installchangelogs -O--no-parallel
   dh_installexamples -O--no-parallel
   dh_installman -O--no-parallel
   dh_installinit -O--no-parallel
   dh_installsystemduser -O--no-parallel
   dh_perl -O--no-parallel
   dh_link -O--no-parallel
   dh_strip_nondeterminism -O--no-parallel
   dh_compress -O--no-parallel
   dh_fixperms -O--no-parallel
   dh_missing -O--no-parallel
   dh_dwz -O--no-parallel
   dh_strip -O--no-parallel
   dh_makeshlibs -O--no-parallel
   dh_shlibdeps -O--no-parallel
   dh_installdeb -O--no-parallel
   dh_gencontrol -O--no-parallel
   dh_md5sums -O--no-parallel
   dh_builddeb -O--no-parallel
dpkg-deb: building package 'mawk' in '../mawk_1.3.4.20200120-2_amd64.deb'.
 dpkg-genbuildinfo
 dpkg-genchanges  >../mawk_1.3.4.20200120-2_amd64.changes
dpkg-genchanges: info: not including original source code in upload
 dpkg-source --after-build .
dpkg-buildpackage: info: binary and diff upload (original source NOT included)
+ date
Mon Jul  8 23:31:39 UTC 2024
+ cd ..
+ ls+ sed mawk_1.3.4.20200120-2_amd64.deb
 -e s,_.*,,
+ apt-cache show mawk=1.3.4.20200120-2
+ sed -e s,^Version: ,,
+ grep ^Version: 
+ binver=1.3.4.20200120-2
+ binnmuver=1.3.4.20200120-2
+ echo 1.3.4.20200120-2
+ sed s,^[0-9]\+:,,
+ binnmuver=1.3.4.20200120-2
+ echo 1.3.4.20200120-2
+ sed s,^[0-9]\+:,,
+ srcver=1.3.4.20200120-2
+ test -f mawk_1.3.4.20200120-2_amd64.deb
+ apt-cache show mawk=1.3.4.20200120-2
+ sed -z s,.*Filename: .*/\(.*\)SHA256: \([0-9a-f]\{64\}\).*,\2  \1,g
+ grep -e ^Filename:  -e ^SHA256: 
+ ls -la
total 612
drwxr-xr-x  3 root root   4096 Jul  8 23:31 .
drwxr-xr-x  3 root root   4096 Jul  8 23:31 ..
-rw-r--r--  1 root root     98 Jul  8 23:31 SHA256SUMS
drwxr-xr-x 10 root root   4096 Jul  8 23:31 mawk-1.3.4.20200120
-rw-r--r--  1 root root   7504 Jul  8 23:31 mawk_1.3.4.20200120-2.debian.tar.xz
-rw-r--r--  1 root root   1032 Jul  8 23:31 mawk_1.3.4.20200120-2.dsc
-rw-r--r--  1 root root   5218 Jul  8 23:31 mawk_1.3.4.20200120-2_amd64.buildinfo
-rw-r--r--  1 root root   1646 Jul  8 23:31 mawk_1.3.4.20200120-2_amd64.changes
-rw-r--r--  1 root root 112536 Jul  8 23:31 mawk_1.3.4.20200120-2_amd64.deb
-rw-r--r--  1 root root 468855 Jan 25  2020 mawk_1.3.4.20200120.orig.tar.gz
+ find . -maxdepth 1 -type f
+ sha256sum ./mawk_1.3.4.20200120-2_amd64.buildinfo ./mawk_1.3.4.20200120-2.dsc ./mawk_1.3.4.20200120-2.debian.tar.xz ./mawk_1.3.4.20200120.orig.tar.gz ./mawk_1.3.4.20200120-2_amd64.changes ./mawk_1.3.4.20200120-2_amd64.deb ./SHA256SUMS
b61ae6b27350589456917dbe62ffaaf32482a5d37ba8231f221a67abb344d8f5  ./mawk_1.3.4.20200120-2_amd64.buildinfo
ef1b62417610115996a9a8aa94c4401a1249be93f9bbf0e6f70e5a6974fdda02  ./mawk_1.3.4.20200120-2.dsc
b772ed2f016b0286980c46cbc1f1f4ae62887ef2aa3dff6ef10cae638f923f26  ./mawk_1.3.4.20200120-2.debian.tar.xz
7fd4cd1e1fae9290fe089171181bbc6291dfd9bca939ca804f0ddb851c8b8237  ./mawk_1.3.4.20200120.orig.tar.gz
36a73273617875d945ddf96d29f3deb13704ef720d84175f5cb9ed2c89484202  ./mawk_1.3.4.20200120-2_amd64.changes
53b3d3852c7fe9f75cd614c2c0c42112bfe8fa5c78e450870d29d0cc1b599b32  ./mawk_1.3.4.20200120-2_amd64.deb
d1575e848fab37361f89814cf511460783095cc82382bf56da20f737719cb506  ./SHA256SUMS
+ tail -v -n+0 SHA256SUMS
==> SHA256SUMS <==
0bc92b1b1851fbdf3343c527fefca7296a7b82ea36b8597924991140b541fc34  mawk_1.3.4.20200120-2_amd64.deb
+ sha256sum -c SHA256SUMS
mawk_1.3.4.20200120-2_amd64.deb: FAILED
sha256sum: WARNING: 1 computed checksum did NOT match
